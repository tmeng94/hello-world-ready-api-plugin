hello-world-ready-api-plugin
==========================

Forked from ready-api-plugin-template.

A sample plugin detecting if the custom property My String of a test step is "Hello World".

Clone the containing repository locally and run in the hello-world-ready-api-plugin folder 

```
mvn install
```

which will create a ready-api-plugin-template-1.0-SNAPSHOT.jar in your target folder. Open Ready! APIs 
Plugin Repository from the main toolbar and choose to load a plugin from file - select this file - which 
will install all the extensions in Ready! API for your musing.

Also see the [maven-soapui-plugin-archetype](https://github.com/olensmar/maven-soapui-plugin-archetype) project 
if you want to create new plugins from scratch.

Please open issues / improvement requests here at GitHub.

Enjoy!