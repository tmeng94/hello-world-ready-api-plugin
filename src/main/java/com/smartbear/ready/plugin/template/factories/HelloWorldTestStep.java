package com.smartbear.ready.plugin.template.factories;

import static com.mongodb.client.model.Filters.eq;

import org.bson.Document;

import com.eviware.soapui.config.TestStepConfig;
import com.eviware.soapui.impl.wsdl.testcase.WsdlTestCase;
import com.eviware.soapui.impl.wsdl.teststeps.WsdlTestStepResult;
import com.eviware.soapui.impl.wsdl.teststeps.WsdlTestStepWithProperties;
import com.eviware.soapui.model.support.DefaultTestStepProperty;
import com.eviware.soapui.model.testsuite.TestCaseRunContext;
import com.eviware.soapui.model.testsuite.TestCaseRunner;
import com.eviware.soapui.model.testsuite.TestStepResult;
import com.eviware.soapui.plugins.auto.PluginTestStep;
import com.mongodb.Block;
import com.mongodb.MongoClient;
import com.mongodb.MongoException;

/**
 * Created by ole on 17/06/14.
 */

@PluginTestStep(typeName = "HelloWorldTestStep", name = "MongoDB Find Demo", description = "Perform a find operation on given MongoDB database.")
public class HelloWorldTestStep extends WsdlTestStepWithProperties {

    public HelloWorldTestStep(WsdlTestCase testCase, TestStepConfig config, boolean forLoadTest) {
        super(testCase, config, false, forLoadTest);
        this.addProperty(new DefaultTestStepProperty("MongoDB Host", this));
        this.addProperty(new DefaultTestStepProperty("MongoDB Port", this));
        this.addProperty(new DefaultTestStepProperty("MongoDB Database Name", this));
        this.addProperty(new DefaultTestStepProperty("MongoDB Collection Name", this));
        this.addProperty(new DefaultTestStepProperty("MongoDB Find Key", this));
        this.addProperty(new DefaultTestStepProperty("MongoDB Find Value", this));
    }

    @Override
    public TestStepResult run(TestCaseRunner testRunner, TestCaseRunContext testRunContext) {
        WsdlTestStepResult result = new WsdlTestStepResult(this);
        String host = this.getPropertyValue("MongoDB Host");
        String portString = this.getPropertyValue("MongoDB Port");
        String database = this.getPropertyValue("MongoDB Database Name");
        String collection = this.getPropertyValue("MongoDB Collection Name");
        String findKey = this.getPropertyValue("MongoDB Find Key");
        String findValue = this.getPropertyValue("MongoDB Find Value");
        
        try {
            int port = Integer.parseUnsignedInt(portString);
            MongoClient mongoClient = new MongoClient(host, port);
            mongoClient.getDatabase(database).getCollection(collection).find(eq(findKey, findValue))
            .forEach((Block<Document>) document -> {
                result.addMessage(document.toJson());
            });
            result.setStatus(TestStepResult.TestStepStatus.OK);
            mongoClient.close();
        }
        catch (NumberFormatException e) {
            result.addMessage("Error: port cannot be parsed as a legal value.");
            result.setStatus(TestStepResult.TestStepStatus.FAILED);
        }
        catch (MongoException e) {
            for (StackTraceElement element: e.getStackTrace()) {
                result.addMessage(element.toString());
            }
            result.setStatus(TestStepResult.TestStepStatus.FAILED);
        }
        return result;
    }
}
